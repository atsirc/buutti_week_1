const process = require('process');
const input = process.argv.slice(2);
const text = input.length === 1 ? input[0].split(' ') : input;
text.splice(-1);
console.log(text.join(' '));