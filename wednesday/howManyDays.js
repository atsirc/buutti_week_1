const process = require('process');
const arg = Number(process.argv.slice(2,3));
if (!Number.isNaN(arg) && arg <= 12 && arg >= 1) {
    const numOfDays = new Date(2021,arg,0).getDate();
    const month = new Date(2021,arg-1).toLocaleString('en-EN', {month:'long'});
    /* If considering years you could add some comment on february... */
    console.log(`${month} has ${numOfDays} days`);
} else {
    console.log('A valid value is a number between 1 and 12');
}