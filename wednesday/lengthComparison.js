const process = require('process');
const names = process.argv.slice(2,5);

if (names.length === 3) {
    names.sort((frst, scnd) => {
        if (frst.length > scnd.length) return -1;
        else if (scnd.length > frst.length) return 1;
        else return 0;
    });
    console.log(names.join(' '));
} else {
    console.log('not enough names. the program requires 3 names');
}