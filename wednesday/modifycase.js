const process = require('process');
const input = process.argv.slice(2);
const task = input[0];
const text = input.length === 2 ? input[1] : input.slice(1).join(' ');
if (task === 'upper') {
    console.log(text.toUpperCase());
} else if (task === 'lower') {
    console.log(text.toLowerCase());
} else {
    console.log('The program requires upper/lower and a string to modify');
}