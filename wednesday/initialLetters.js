const process = require('process');
const names = process.argv.slice(2,5);

if (names.length === 3) {
    const letters = names.map(n => n[0].toLowerCase());
    console.log(letters.join('.'));
} else {
    console.log('Not enough names. The program requires 3 names');
}