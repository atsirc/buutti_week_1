const isActive = false;
const balance = -10;
const checkBalance = false;

if (checkBalance) {
    if (isActive) {
        if (balance>0) {
            console.log(balance);
        } else if (balance === 0) {
            console.log('Your account is empty!');
        } else {
            console.log('Your balance is negative');
        }
    } else {
        console.log('Your account is not active');
    }
} else {
    console.log('Have a nice day!');
}