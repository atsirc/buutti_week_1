const process = require('process');
const input = process.argv.slice(2);
const toBeReplaced = input[0];
const replacement = input[1];
let text = input.length === 3 ? input[2] : input.slice(2).join(' ');
text = text || '';
if (text.length > 0) {
    text = text.replaceAll(toBeReplaced, replacement);
    console.log(text);
} else {
    console.log('The program requires a value that should be replaced, the replacement value and a string');
}