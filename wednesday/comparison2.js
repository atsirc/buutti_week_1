const process = require('process');
const args = process.argv.slice(2);
if (args.length < 3) {
    console.log('The program requires 3 numbers');
} else {
    const numbers = args.slice(0,3).map(Number);
    if (numbers.every(n => !Number.isNaN(n))) {
        const min = Math.min(...numbers);
        const max = Math.max(...numbers);
        const [number_1,number_2,number_3] = numbers;
        if (numbers.every(n => n===number_1)) {
            console.log('All values are equal');
        } else {
            switch (min) {
                case number_1: console.log('Number_1 is smallest'); break;
                case number_2: console.log('Number_2 is smallest'); break;
                case number_3: console.log('Number_3 is smallest'); break; 
                default: console.log('Something went wrong here');
            }
            switch (max) {
                case number_1: console.log('Number_1 is greatest'); break;
                case number_2: console.log('Number_2 is greatest'); break;
                case number_3: console.log('Number_3 is greatest'); break; 
                default: console.log('Something went wrong here');
            }
        }
    } else {
        console.log('All values have to be numbers');
    }
}