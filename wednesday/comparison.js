const process = require('process');
const args = process.argv.slice(2);

if (args.length<2) {
    console.log('The program needs two numbers');
} else {
    const [frst, scnd] = args.slice(0,2).map(n => Number(n));
    const thrd = args[2];
    if (frst === scnd) {
        if (thrd !== 'Hello world') {
            console.log('they are equal');
        } else {
            console.log('Yay you guessed the password!');
        }
    } else {
        console.log(`${(Math.max(frst,scnd)===frst) ? 'a' : 'b'} is greater`);
    }
}