const arr = [1,4,6,32,25,16,31,15,18,2,7]
let max = Number.MIN_SAFE_INTEGER
let second_biggest = Number.MIN_SAFE_INTEGER
for (const num of arr) {
    if (num > max){
        second_biggest = max 
        max = num
    } else if (num > second_biggest) {
        second_biggest = num
    }
}

console.log(max)
console.log(second_biggest)