const arr = ['banaani', 'omena', 'mandariini', 'appelsiini', 'kurkku', 'tomaatti', 'peruna'];
console.log('\Original array:')
arr.forEach((n)=>console.log(n));

console.log('\nContains the letter r')
arr.forEach((n)=> {
    if (n.includes('r')) {
        console.log(n);
    }
})
sortedArr= arr.sort()
console.log('\nSorted array:')
sortedArr.forEach(n => console.log(n))

shiftedArr = arr.slice(1)
console.log('\nShifted array:')
shiftedArr.forEach(n => console.log(n))

//or shift the original array...
//arr.shift()

console.log('\nNew array with sipuli')
newArr = [...arr, 'sipuli'];
newArr.forEach(n => console.log(n))