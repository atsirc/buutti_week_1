for (i=1; i<=100; i++) {
    if (i%3===0 && i%5===0) {
        console.log('FizzBuzz')
    } else {
        if (i%3===0){
            console.log('Fizz')
        }
        if (i%5===0) {
            console.log('Buzz')
        }
    }
}

for (i=1; i<=100; i++) {
    let str = ''
    if (i%3===0){
        str += 'Fizz'
    }
    if (i%5===0) {
        str += 'Buzz'
    }
    if (str.length > 0) console.log(str)
}