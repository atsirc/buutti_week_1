const process = require('process');

let values = [];

// takes the two first numbers
process.argv.forEach((val, i) => {
    val = Number(val);
    if (val && values.length < 2) {
        values.push(Number(val));
    }
});

//  if not enough or no numbers add 10 and 20
if (values.length !== 2){
    values = [10, 20];
}

//  sum with reducer, pretty unnescessary
const sum = values.reduce((a,b) => {return a +b});
console.log(`Sum of the values ${sum}`);

//  difference with reducer, unnescessary
const diff = values.reduce((a,b) => a - b);
console.log(`Difference between the values ${diff}`);

//  fraction 
const fraction = values[0] / values[1];
console.log(`Fraction of the values ${fraction}`);

//  product with reducer
const prod = values.reduce((a,b) => {return a*b});
console.log(`Product of the values ${prod}`);

//  average from earlier sum variable
const average = sum / 2;
console.log(`Averge of the values ${average}`);

//  exponential
const exp = Math.pow(values[0], values[1]);
console.log(`Exponential of the values ${exp}`);

//  modulo, the most useful one
console.log(`Modulo of the values ${values[0]%values[1]}`);

