const process = require('process');

const strings = process.argv.slice(2);

//   Checks that a string has been added. If not adds own strings
if (strings.length === 0) {
    const own_strings = ['   This is a test.', ' This would be another test', 'Forget about it...'];
    strings.push(...own_strings);
}

//  Trims string, then cuts it at index 20 and makes first lette lower case
const modifyString = (val) => {
    let modifiedString = val.trim();
    modifiedString = modifiedString.substr(0,20);
    modifiedString = modifiedString[0].toLowerCase() + modifiedString.substr(1,20);
    return modifiedString;
};

strings.forEach((str) => { console.log( modifyString(str) ) });