/* Part a */
const playHearts = () => { console.log('Playing hearts') }
let playerCount = 4;
if (playerCount === 4) {
    playHearts();
}

/* Part b */
const Mark = {
    'hasIcecream': null,
    'isHappy': null,
    'isStressed': null
};
Mark.hasIcecream = true;
Mark.isStressed = false;

if (Mark.hasIcecream && !Mark.isStressed) {
    Mark.isHappy = true;
} else {
    Mark.isHappy = false;
}
console.log('Is Mark happy? ' + Mark.isHappy);

/* Part c */
let isBeachDay = false;
let isShining = true;
let isRaining = false;
let temperature = 20;
if (isShining && !isRaining && temperature >= 20) {
    isBeachDay = true;
}
console.log('Is it a beach day: ' + isBeachDay);

/* Part d */
let ArinIsHappy = false;
let Suzy = true;
let Dan = true;
let time = 'Tuesday night';
if (time == 'Tuesday night' && (Suzy || Dan) && !(Suzy && Dan)) {
    ArinIsHappy = true;
}
console.log('Arin is happy: ' + ArinIsHappy);