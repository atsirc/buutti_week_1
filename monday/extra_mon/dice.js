const process = require('process')
const readline = require('readline')
/* Truly important to add ./ otherwise node won't find the file */
const game = require('./dice_game')

const runApplication = (interface, sides) => {
    interface.question('Press <enter> for a dice roll, type "q" + <enter> for exit ', (answer) => {
        if (answer.length === 0) {
            const result = game.rollDice(sides)         
            console.log(result)
        } else if(answer === "q") {
            interface.close()
            return
        }
        runApplication(interface, sides)
    })
}
const init = () => {
    const args = process.argv.slice(2)
    if (args.length === 0 ) {
        console.log('No input given.')
    } else {
        let sides = args[0]
        if (sides[0]==='D' && sides.length > 1) {
            sides = Number(sides.substring(1))
            if (sides < 3) {
                console.log('The dice was too small, you were given 6-sided dice intead')
                sides = 6
            }
            if (sides <= 100) {
                const interface = readline.createInterface({
                    input: process.stdin,
                    output: process.stdout
                })
                runApplication(interface, sides)
            }
            else {
                console.log('Not a valid value. Valid values start with D followd by a number between 3 and 100. Ex: D10')
            }
        } else {
            console.log('Not a valid value. Valid values start with D followd by a number between 3 and 100. Ex: D10')
        }
    }
}

init()