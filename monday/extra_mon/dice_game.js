const addedMsg = (sides, result) => {
    if (result === sides) {
        return 'Yes! '
    } else if (result === 1) {
        return 'Ouch! '
    } else {
        return ''
    }
}
const rollDice = (sides) => {
    let result = 0
    if (sides === 3) {
        result = Math.floor(((Math.random()*6))/2) + 1
    } else {
        result = Math.floor(Math.random()*sides)+1
    }
    return `${addedMsg(sides, result)}The roll is ${result}`
}
module.exports = {
    rollDice
}
