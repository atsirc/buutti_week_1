const readerline = require('readline')
const interface = readerline.createInterface({
    input: process.stdin,
    output: process.stdout
})
interface.question('Kuinka mones kävijä tänään? ', (answer) => {
    if (answer % 2000 === 0 && answer > 0) {
        console.log('Saa lahjakortin!!!')
    } else if (answer >= 1000 && answer % 25 === 0) {
        console.log('Saa ilmapallon!') 
    } else {
        console.log('Ei saa mitään. :´(')
    }
    interface.close()
})