const str1 = 'This only works ';
const str2 = 'if the strings have different lengths';

const str_sum = (str1, str2) => {
    return str1+str2;
}
console.log(str_sum(str1,str2));

//  length of the two strings
console.log(`str1 length: ${str1.length}, str2 length: ${str2.length}`);

//  average length of the strings
const average = (str1.length + str2.length) / 2;
console.log(`Average length of strings ${average}`);

if (str1.length < average) {
    console.log(str1);
}
if (str2.length < average) {
    console.log(str2);
}
if (str_sum(str1,str2).length < average) {
    console.log(str_sum(str1,str2));
}